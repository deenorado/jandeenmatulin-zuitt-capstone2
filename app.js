const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

// Set up middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Set up routes
app.use("/", require("./routes/indexRoutes"));
app.use("/users", require("./routes/userRoutes"));
app.use("/admin", require("./routes/adminRoutes"));

// Set up DB connection, then start to listen to port after successful connection
mongoose
  .connect(
    "mongodb+srv://admin:admin@b106.uaufn.mongodb.net/capstone2?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    }
  )
  .then(
    app.listen(process.env.PORT || 4000, console.log(`Connected to port 4000`))
  );
