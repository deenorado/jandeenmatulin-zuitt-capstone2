const express = require("express");
const auth = require("../routes/auth");

// Set up controllers
const userController = require("../controllers/userController");

const router = express.Router();

router.post("/login", (req, res) => {
  return userController
    .verifyUser(req, res)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.post("/signup", (req, res) => {
  return userController
    .createUser(req.body)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.get("/profile", auth.isLoggedIn, (req, res) => {
  return userController
    .getProfile(req.decoded)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.put("/change-password", auth.isLoggedIn, (req, res) => {
  return userController
    .changePassword(req)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.post("/checkout", auth.isLoggedIn, (req, res) => {
  return userController
    .createOrder(req)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.get("/view-orders", auth.isLoggedIn, (req, res) => {
  return userController
    .viewOrders(req.decoded)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

module.exports = router;
