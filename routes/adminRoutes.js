const express = require("express");
const auth = require("../routes/auth");

// Set up controllers
const adminController = require("../controllers/adminController");

const router = express.Router();

router.post("/login", (req, res) => {
  return adminController
    .verifyAdmin(req, res)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.put("/set-admin", auth.isLoggedIn, auth.isAdmin, (req, res) => {
  return adminController
    .setAdmin(req.body)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.post("/create-product", auth.isLoggedIn, auth.isAdmin, (req, res) => {
  return adminController
    .createProduct(req.body)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.put("/update/:productid", auth.isLoggedIn, auth.isAdmin, (req, res) => {
  return adminController
    .updateProduct(req.params.productid, req.body)
    .then((result, err) => {
      err ? res.send(err) : res.send(result);
    });
});

router.post(
  "/archive/:productid",
  auth.isLoggedIn,
  auth.isAdmin,
  (req, res) => {
    return adminController
      .archiveProduct(req.params.productid)
      .then((result, err) => (err ? res.send(err) : res.send(result)));
  }
);

router.get("/all-orders", auth.isLoggedIn, auth.isAdmin, (req, res) => {
  return adminController
    .viewAllOrders()
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.delete(
  "/delete-order/:orderid",
  auth.isLoggedIn,
  auth.isAdmin,
  (req, res) => {
    return adminController
      .deleteOrder(req.params.orderid)
      .then((result, err) => (err ? res.send(err) : res.send(result)));
  }
);

module.exports = router;
