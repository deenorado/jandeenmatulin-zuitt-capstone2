const jwt = require("jsonwebtoken");

module.exports.generateAccessToken = (params) => {
  const { email, password, isAdmin } = params;
  const user = {
    email,
    password,
    isAdmin,
  };
  return jwt.sign(user, process.env.SECRET, { expiresIn: "1d" });
};

module.exports.isLoggedIn = (req, res, next) => {
  let token = req.headers["authorization"];

  if (token !== null) {
    token = token.split(" ")[1];
    jwt.verify(token, process.env.SECRET, (result) => {
      req.decoded = jwt.decode(token, { complete: true }).payload;
      next();
    });
  }
};

module.exports.isAdmin = async (req, res, next) => {
  if (req.decoded.isAdmin) return next();
  else res.send("Failed to authenticate admin privileges");
};
