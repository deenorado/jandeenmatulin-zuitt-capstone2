const express = require("express");

// Set up controllers
const indexController = require("../controllers/indexController");

const router = express.Router();

router.get("/products", (req, res) => {
  return indexController
    .viewAllProducts()
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

router.get("/products/:productid", (req, res) => {
  return indexController
    .viewProduct(req.params.productid)
    .then((result, err) => (err ? res.send(err) : res.send(result)));
});

module.exports = router;
