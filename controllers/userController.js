const bcrypt = require("bcrypt");
const auth = require("../routes/auth");

// Set up models
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.createUser = async (params) => {
  const { email, password, fullName, about } = params;
  // Check if another user with the same email address already exists
  const duplicateUser = await User.findOne({ email });

  if (duplicateUser !== null)
    return { error: "Email address is already in use" };

  return User.create({
    email,
    fullName,
    about,
    password: bcrypt.hashSync(password, 10),
  }).then((result, err) =>
    err ? { error: "Unable to sign user up" } : result
  );
};

module.exports.verifyUser = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  const token = await auth.generateAccessToken(req.body);

  if (!user) return { error: "No user with those credentials found." };
  else if (!bcrypt.compareSync(password, user.password))
    return { error: "Incorrect password" };

  return {
    user,
    token,
  };
};

module.exports.getProfile = async (params) => {
  const user = await User.find({ email: params.email });

  return !user ? { error: "No user with those credentials found." } : user;
};

module.exports.changePassword = async (params) => {
  return await User.findOneAndUpdate(
    { email: params.decoded.email },
    { password: bcrypt.hashSync(params.body.password, 10) },
    { new: true }
  ).then((result, err) =>
    err ? { error: "Failed to update password" } : result
  );
};

module.exports.createOrder = async (params) => {
  // Checks if user is an admin
  if (params.decoded.isAdmin) return "No admins allowed to checkout";

  const { name, quantity } = params.body;

  const user = await User.find({ email: params.decoded.email });
  const product = await Product.find({ name });

  // Check if product exists in database
  if (!product.length)
    return { error: "No product with those specifications found" };

  let totalAmount = product[0].price * quantity;

  // Stores product to push to order
  const productToAdd = {
    name: product[0].name,
    description: product[0].description,
    price: product[0].price,
    quantity,
    productId: product[0].id,
  };

  // Checks if there is an existing user order, and if so, appends new product to the same order
  if (user[0].order[0]) {
    // Looks for the order using the existing order id assigned to user
    const thisOrder = await Order.findById(user[0].order[0]._id);
    const existingProductIndex = thisOrder.products.findIndex((prod) => {
      return prod.name == name;
    });

    // Check if the product to be added is already in the order, if so, add additional products to existing quantity and price
    if (existingProductIndex >= 0) {
      if (quantity === -1) {
        if (thisOrder.products[existingProductIndex].quantity === 1) {
          return await Order.findOneAndUpdate(
            { _id: user[0].order[0]._id, "products.name": name },
            {
              $inc: {
                totalAmount: -product[0].price,
              },
              $pull: { products: { name: name } },
            },
            { new: true }
          ).then((result, err) =>
            err ? { error: "Failed to update order" } : result
          );
        } else
          return await Order.findOneAndUpdate(
            { _id: user[0].order[0]._id, "products.name": name },
            {
              $inc: {
                totalAmount: -product[0].price,
                "products.$.quantity": -1,
              },
            },
            { new: true }
          ).then((result, err) =>
            err ? { error: "Failed to update order" } : result
          );
      } else
        return await Order.findOneAndUpdate(
          { _id: user[0].order[0]._id, "products.name": name },
          {
            $inc: {
              totalAmount,
              "products.$.quantity": quantity,
            },
          },
          { new: true }
        ).then((result, err) =>
          err ? { error: "Failed to update order" } : result
        );
    } else if (existingProductIndex < 0) {
      return await Order.findByIdAndUpdate(
        user[0].order[0]._id,
        {
          $inc: { totalAmount },
          $push: { products: productToAdd },
        },
        { new: true }
      ).then((result, err) => (err ? { error: err } : result));
    }
    // If there are no existing orders, create a new order with the product and quantity provided
  } else {
    const newOrder = await Order.create({
      userId: user[0]._id,
      totalAmount,
      products: productToAdd,
    });

    const updateUser = await User.findByIdAndUpdate(newOrder.userId, {
      order: newOrder._id,
    });

    if (newOrder && updateUser) {
      return true;
    }
  }
};

module.exports.viewOrders = async (params) => {
  const { email } = params;
  const user = await User.find({ email });
  const orderId = user[0].order[0]._id;

  if (orderId === null)
    return { error: "You have no active orders at the moment" };

  return Order.findById(orderId).then((result, err) =>
    err ? { error: "Unable to retrieve your orders at this time" } : result
  );
};
