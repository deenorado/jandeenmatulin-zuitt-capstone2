// Set up models
const Product = require("../models/Product");

module.exports.viewAllProducts = () => {
  return Product.find(
    { isActive: true },
    { _id: 1, name: 1, description: 1, price: 1 },
    {
      new: true,
    }
  ).then((result, err) =>
    err ? { error: "Something went wrong with fetching products" } : result
  );
};

module.exports.viewProduct = (params) => {
  return Product.findById(params).then((result, err) =>
    err ? { error: "Something went wrong with fetching product" } : result
  );
};
