const auth = require("../routes/auth");
const bcrypt = require("bcrypt");

// Set up models
const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");

module.exports.verifyAdmin = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) return { error: "No admin registered with that email address" };
  else if (!bcrypt.compareSync(password, user.password))
    return { error: "Incorrect password" };
  else if (!user.isAdmin) return { error: "This user is not an admin" };

  const token = await auth.generateAccessToken(user);
  return { accessToken: token, isAdmin: true };
};

module.exports.setAdmin = async (params) => {
  const { email } = params;
  const user = await User.findOne({ email });
  const checkIfAdmin = await user.isAdmin;

  if (checkIfAdmin) {
    return "User is already an admin";
  } else if (!checkIfAdmin) {
    return User.findOneAndUpdate({ email }, { isAdmin: true }).then(
      (result, err) =>
        err ? { error: "Failed to set admin privileges" } : result
    );
  }
};

module.exports.createProduct = async (params) => {
  const { name, description, price } = params;
  const duplicateProduct = await Product.find({ name });

  if (duplicateProduct.length) return { error: "Duplicate product found" };

  return Product.create({
    name,
    description,
    price,
  }).then((result, err) =>
    err ? { error: "Creating a product listing failed" } : result
  );
};

module.exports.updateProduct = (id, params) => {
  const { name, description, price } = params;
  return Product.findByIdAndUpdate(id, { name, description, price }).then(
    (result, err) =>
      err ? { error: "Failed to update product listing" } : result
  );
};

module.exports.archiveProduct = (id) => {
  return Product.findByIdAndUpdate(id, { isActive: false }).then(
    (result, err) => (err ? { error: "Failed to archive product" } : result)
  );
};

module.exports.viewAllOrders = async () => {
  // Get all orders from database
  const orders = await Order.find({});

  try {
    if (!orders.length) return { error: "No active orders at this time" };
    else return orders;
  } catch (err) {
    return { error: "Unable to display orders at this time" };
  }
};

module.exports.deleteOrder = async (id) => {
  const order = await Order.findById(id);
  const userId = order.userId;

  return User.findOneAndUpdate(
    { _id: userId },
    {
      $pop: { order: -1 },
    },
    { new: true },
    (err, result) => (err ? err : result)
  ).then(
    Order.findByIdAndDelete(id).then((result, err) => (err ? err : result))
  );
};
