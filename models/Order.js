const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  purchasedOn: {
    type: Date,
    default: Date.now(),
  },

  totalAmount: {
    type: Number,
  },

  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
      },

      name: {
        type: mongoose.Schema.Types.String,
        ref: "Product",
      },

      description: {
        type: mongoose.Schema.Types.String,
        ref: "Product",
      },

      price: {
        type: mongoose.Schema.Types.Number,
        ref: "Product",
      },

      quantity: {
        type: Number,
      },
    },
  ],
});

module.exports = mongoose.model("Order", orderSchema);
